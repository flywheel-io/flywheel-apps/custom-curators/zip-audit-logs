#!/usr/bin/env python
import logging
from pathlib import Path

import flywheel
from flywheel_gear_toolkit import GearToolkitContext
from flywheel_gear_toolkit.utils import datatypes
from flywheel_gear_toolkit.utils.curator import get_curator

from fw_gear_zip_audit_logs.parser import parse_config

log = logging.getLogger(__name__)


def curate(
    context: GearToolkitContext,
    project: flywheel.Project,
    curator_path: datatypes.PathLike,
    **kwargs,
) -> None:  # pragma: no cover
    curator = get_curator(context, curator_path, **kwargs)

    curator.curate_container(project)


def main(gear_context: GearToolkitContext) -> None:  # pragma: no cover
    project = parse_config(gear_context)

    log.info(f"Curating project {project.label}")
    curator_path = Path("./fw_gear_zip_audit_logs/zip_audit_logs.py")

    curate(
        gear_context,
        project,
        curator_path,
    )


if __name__ == "__main__":  # pragma: no cover
    with GearToolkitContext() as gear_context:
        gear_context.init_logging()
        main(gear_context)
