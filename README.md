# Zip Audit Logs

## Summary

Zip audit logs attached to project

## Usage

Run as a project analysis to merge audit logs into a new or existing zip archive.

### Configuration

* __debug__ (boolean, default False): Include debug statements in output.

## Contributing

For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).
