import logging
import tempfile
import zipfile
from pathlib import Path

import backoff
import requests
from flywheel_gear_toolkit.utils.curator import HierarchyCurator

log = logging.getLogger(__name__)


@backoff.on_exception(backoff.expo, requests.exceptions.ConnectionError, max_time=100)
def delete_file(url, project_id, file_name, api_key):
    requests.request(
        "DELETE",
        f"{url}/projects/{project_id}/files/{file_name}",
        headers={"Authorization": f"scitran-user {api_key}"},
    )


@backoff.on_exception(backoff.expo, requests.exceptions.ConnectionError, max_time=100)
def upload_file(project, path):
    project.upload_file(path)


@backoff.on_exception(backoff.expo, requests.exceptions.ConnectionError, max_time=100)
def download_file(file_, path):
    file_.download(str(path / file_.name))


class Curator(HierarchyCurator):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.config.stop_level = "project"
        self.context.client._fw.api_client.set_default_query_param("exhaustive", "true")
        self.url = self.context.client._fw.api_client.configuration.host
        self.api_key = self.context.client.api_client.configuration.api_key[
            "Authorization"
        ]

    def curate_project(self, project):
        """Download all audit_logs, zip, re-upload, delete originals."""
        log.info(f"Downloading audit logs for project {project.label}")
        project = project.reload()
        zip_name = "zipped_audit_logs.zip"
        existing_zip_log = project.get_file(zip_name)
        with tempfile.TemporaryDirectory() as tmp_dir:
            path = Path(tmp_dir)
            if existing_zip_log:
                log.info("Using existing zipped_audit_logs")
                download_file(existing_zip_log, path)
            num = len(project.files)
            i = 0
            for file_ in project.files:
                if i % 100 == 0:
                    log.info(f"Downloading {i}/{num}")
                if file_.name.startswith("audit_log") and file_.name.endswith(".csv"):
                    download_file(file_, path)
                i += 1
            log.info(f"Compressing all files into {zip_name}")
            with zipfile.ZipFile(str(path / zip_name), "a") as zipf:
                log.info(f"Existing files in archive: {len(zipf.filelist)}")
                for file_ in path.glob("*.csv"):
                    zipf.write(file_, arcname=file_.name)
            log.info(f"Uploading {zip_name}")
            upload_file(project, str(path / zip_name))
            log.info("Deleting existing audit logs")
            i = 0
            for file_ in project.files:
                if i % 100 == 0:
                    log.info(f"Deleting {i}/{num}")
                if file_.name.startswith("audit_log") and file_.name.endswith(".csv"):
                    delete_file(self.url, project.id, file_.name, self.api_key)
                    # Gives a 500 error, not sure why
                    # project.delete_file(file_.name)

                i += 1
        log.info(f"Project {project.label} done")
