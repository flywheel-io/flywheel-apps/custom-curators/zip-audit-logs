"""Parser module to parse gear config.json."""
from typing import Any

from flywheel_gear_toolkit import GearToolkitContext


# This function mainly parses gear_context's config.json file and returns relevant inputs and options.
def parse_config(
    gear_context: GearToolkitContext,
) -> Any:
    project = gear_context.get_destination_parent()
    return project
